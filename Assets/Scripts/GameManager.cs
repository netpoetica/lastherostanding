﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Photon.Pun;
using Photon.Realtime;

namespace com.sporeservant.lastherostanding
{
    public class GameManager : MonoBehaviourPunCallbacks
    {
        public GameObject playerPrefab;
        public Canvas canvas;
        public HUD hud;

        // Game Manager will tell the master client player board things, which it will RPC to the other boards.
        private PlayerBoard masterClientPlayerBoard
        {
            get
            {
                PlayerBoard[] playerBoards = GameObject.FindObjectsOfType<PlayerBoard>();
                Debug.Log("Found " + playerBoards.Length + " player boards.");
                foreach (PlayerBoard pb in playerBoards)
                {
                    if (pb.photonView.Owner == PhotonNetwork.MasterClient)
                    {
                        return pb;
                    }
                }
                return null;
            }
        }

        private void Start()
        {
            Screen.fullScreen = false;
            Debug.Log("GameManager::Start");

            if (hud == null)
                hud = GameObject.FindObjectOfType<HUD>();

            if (PhotonNetwork.CurrentRoom != null)
            {
                Debug.LogFormat("GameManager::PlayerCount = {0}", PhotonNetwork.CurrentRoom.PlayerCount);
                hud.ShowTurnIndicator("Waiting for opponent", false);
                if (PhotonNetwork.CurrentRoom.PlayerCount == 2)
                {
                    Debug.Log("2 players found - starting game.");
                    hud.HideTurnIndicator();

                    // Don't show player their board until both players are in.
                    if (playerPrefab != null)
                    {
                        PhotonNetwork.Instantiate(this.playerPrefab.name, Vector3.zero, Quaternion.identity, 0);
                    }

                    PlayerBoard playerBoard = masterClientPlayerBoard;
                    if (playerBoard != null)
                        playerBoard.StartGame();
                }
            }
        }

        private void LoadArena()
        {
            if (!PhotonNetwork.IsMasterClient)
            {
                Debug.LogError("Trying to load a level but we are not the master client.");
            }

            Debug.LogFormat("Loading level: {0}", PhotonNetwork.CurrentRoom.PlayerCount);
            PhotonNetwork.LoadLevel("Board");
        }

        public override void OnPlayerEnteredRoom(Player newPlayer)
        {
            base.OnPlayerEnteredRoom(newPlayer);
            Debug.LogFormat("Player Entered Room: {0}", newPlayer.NickName);

            if (PhotonNetwork.IsMasterClient)
            {
                Debug.LogFormat("Master client loading board.");
                LoadArena();
            }
        }

        public override void OnPlayerLeftRoom(Player otherPlayer)
        {
            base.OnPlayerLeftRoom(otherPlayer);
            Debug.LogFormat("Other player left room: {0}", otherPlayer.NickName);

            if (PhotonNetwork.IsMasterClient)
            {
                Debug.LogFormat("Master client left room: {0}", PhotonNetwork.IsMasterClient);
                // TODO: Is this necessary?
                LoadArena();
            }
        }

        public override void OnLeftRoom()
        {
            base.OnLeftRoom();
            SceneManager.LoadScene(0);
        }

        // Button Handlers
        public void LeaveRoom()
        {
            PhotonNetwork.LeaveRoom();
        }

        // Player Turn -> Opponent Turn -> Combat
        public void EndTurn()
        {
            switch (masterClientPlayerBoard.GetGameState())
            {
                case GameState.PLAYER_TURN:
                    masterClientPlayerBoard.SetGameState(GameState.OPPONENT_TURN);
                    break;
                case GameState.OPPONENT_TURN:
                    masterClientPlayerBoard.SetGameState(GameState.COMBAT);
                    break;
                case GameState.COMBAT:
                    masterClientPlayerBoard.SetGameState(GameState.PLAYER_TURN);
                    break;
                default: break;
            }
        }
    }
}

