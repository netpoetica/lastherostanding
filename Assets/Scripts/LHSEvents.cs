﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Photon.Realtime;

namespace com.sporeservant.lastherostanding
{
    [System.Serializable]
    public class CardPlacedEvent : UnityEvent<CardSO, CardHolder, Player>
    {
    }
}