﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

namespace com.sporeservant.lastherostanding
{
    public class CardDisplay : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IBeginDragHandler, IEndDragHandler, IDragHandler
    {
        public GameObject front;
        public GameObject back;

        public CardSO cardData;
        public Canvas parentCanvas;
        public CanvasGroup canvasGroup;

        public TextMeshProUGUI cardName;
        public TextMeshProUGUI cardDescription;
        public TextMeshProUGUI cardType;
        public TextMeshProUGUI hitPoints;
        public TextMeshProUGUI power;
        public TextMeshProUGUI toughness;

        public Image cardImage;
        public Image itemSlots;

        public Sprite[] itemSlotImages;

        // Position data for drag and drop
        private RectTransform rectTransform;
        private Vector2 positionBeforeDrag;
        private Vector2 scaleBeforeFocus;

        private bool IsInPlay = false;

        public void Awake()
        {
            rectTransform = GetComponent<RectTransform>();
            parentCanvas = GetComponentInParent<Canvas>();
            canvasGroup = GetComponent<CanvasGroup>();
        }

        public void ShowBack()
        {
            front.SetActive(false);
            back.SetActive(true);
        }

        public void ShowFront()
        {
            front.SetActive(true);
            back.SetActive(false);
        }

        public void SetInPlay(bool isInPlay)
        {
            IsInPlay = isInPlay;
        }

        public void SetScale(Vector2 scale)
        {
            rectTransform.localScale = scale;
        }


        public void SetPosition(Vector2 pos)
        {
            rectTransform.anchoredPosition = pos;
        }

        public void SetCardData(CardSO data)
        {
            cardData = data;
            Init();
        }

        // Start is called before the first frame update
        void Start()
        {
            if (cardData != null)
            {
                Init();
            }
        }

        private void Init()
        {
            if (cardData == null)
            {
                return;
            }

            toughness.enabled = false;
            power.enabled = false;
            hitPoints.enabled = false;

            // Static, unchanging properties
            cardName.text = cardData.cardName;
            cardDescription.text = cardData.cardDescription;
            cardImage.sprite = cardData.cardImage;

            UpdateCardDisplay();
            SetCardType();
            SetItemSlotImage();
        }

        // This needs to be called whenever data changes
        public void UpdateCardDisplay()
        {
            if (cardData == null)
            {
                return;
            }

            hitPoints.text = cardData.health.ToString();
            power.text = cardData.power.ToString();
            toughness.text = cardData.toughness.ToString();
        }

        void SetCardType()
        {
            switch (cardData.cardType)
            {
                case CardType.ARMOR:
                    cardType.text = "ARMOR";
                    toughness.enabled = true;
                    break;
                case CardType.ITEM:
                    cardType.text = "ITEM";
                    if (cardData.toughness > 0)
                        toughness.enabled = true;
                    if (cardData.power > 0)
                        power.enabled = true;
                    if (cardData.health > 0)
                        hitPoints.enabled = true;
                    break;
                case CardType.WEAPON:
                    power.enabled = true;
                    cardType.text = "WEAPON";
                    break;
                case CardType.HERO:
                    cardType.text = "HERO";
                    toughness.enabled = true;
                    power.enabled = true;
                    hitPoints.enabled = true;
                    break;
            }
        }

        void SetItemSlotImage()
        {
            switch (cardData.itemCount)
            {
                case 1:
                    itemSlots.sprite = itemSlotImages[0];
                    break;
                case 2:
                    itemSlots.sprite = itemSlotImages[1];
                    break;
                case 3:
                    itemSlots.sprite = itemSlotImages[2];
                    break;
                default:
                    itemSlots.enabled = false;
                    break;
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            // Debug.Log("Mouse/touch ptr down.");
            scaleBeforeFocus = rectTransform.localScale;
            rectTransform.localScale = scaleBeforeFocus * new Vector2(2.0f, 2.0f);
        }


        public void OnPointerUp(PointerEventData eventData)
        {
            rectTransform.localScale = scaleBeforeFocus;
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            // Debug.Log("Begin drag");
            if (IsInPlay) return;

            canvasGroup.blocksRaycasts = false;
            canvasGroup.alpha = 0.75f;
            positionBeforeDrag = rectTransform.anchoredPosition;
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            // Debug.Log("End drag");
            if (!IsInPlay)
            {
                rectTransform.anchoredPosition = positionBeforeDrag;
            }
            canvasGroup.alpha = 1f;
            canvasGroup.blocksRaycasts = true;
        }

        public void OnDrag(PointerEventData eventData)
        {
            // Debug.Log("on drag");
            if (IsInPlay) return;
            rectTransform.anchoredPosition += eventData.delta / parentCanvas.scaleFactor;
        }

    }

}
