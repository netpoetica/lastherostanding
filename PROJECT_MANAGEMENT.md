Todo
----
- probably need a CardModifier class to sit on cardDisplays to avoid replacing hero stats when cards go away
- when a hero dies, how do we handle the number of item slots changing?
	- maybe items last 1 turn only?
- figure out how to handle all of the adding of played cards
- figure out how to handle hit points damage
- create some kind of win/lose screen
- use GPS to locate nearby players?
- multiplayer synchronization

Done
----
- add waiting for opponent screen
- change number of item slots visible depending on hero itemCount
- dynamically load images/textures from ScriptableObjects
- create a script to spread cards out in a slight circle depending on how many cards in hand
- create the concept of a Turn
	- player 1 goes, player 2 goes, combat happens, repeat


Not Gonna Happen
----------------
- ask how many heroes they want to play with when they create a room
- create the concept of a "table per hero" aka different table per turn
	- this means there will be N different PlayerCardsInPlay panels for number of heroes played
- create the ability to draw cards


