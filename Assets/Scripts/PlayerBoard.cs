﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

namespace com.sporeservant.lastherostanding
{
    public enum GameState
    {
        WAITING_FOR_OPPONENT,
        // player = MasterClient
        PLAYER_TURN,
        // opponent = !MasterClient
        OPPONENT_TURN,
        COMBAT,
        PLAYER_WIN,
        OPPONENT_WIN,
        DRAW
    }

    public struct PlayerBoardState
    {
        public Player player;
        public CardSO hero;
        public CardSO weapon;
        public CardSO armor;
        public List<CardSO> items;
        public HeroStats calculatedHeroStats;
        public List<CardSO> deadHeroes;
    }

    public struct HeroStats
    {
        public int heroHealth;
        public int heroToughness;
        public int heroPower;
        public int heroDamage;
    }

    public struct CombatRound
    {
        public int damageToPlayer;
        public int damageToOpponent;
    }

    public class PlayerBoard : MonoBehaviourPun
    {
        #region Public Properties
        public GameObject cardPrefab;
        public GameObject playerHand;
        public HeroBonusOverlay heroBonusOverlay;
        public CardHolder heroCardHolder;
        public CardHolder weaponCardHolder;
        public CardHolder armorCardHolder;
        public CardHolder[] itemCardHolders;
        #endregion Public Properties

        #region Private Properties
        private HUD hud;

        private List<CardDisplay> cardsInHand = new List<CardDisplay>();

        private PlayerBoardState playerBoardState = new PlayerBoardState();

        // Game Board
        private const uint NUM_ITEM_CARDS = 7;
        private const uint MAX_ITEM_CARDS = 3;
        private const int MAX_CARDS_IN_HAND = 7;

        private List<CardSO> heroes;
        private List<CardSO> drawnHeroes = new List<CardSO>();
        private CardSO[] weapons;
        private CardSO[] armor;
        private CardSO[] items;

        [SerializeField]
        private GameState gameState;
        [SerializeField]
        private bool bFirstCombatOccurred = false;
        #endregion Private Properties

        //
        // Unity Lifecycle Functions
        //
        #region Unity Lifecycle Functions
        private void Awake()
        {
            this.transform.SetParent(GameObject.Find("Canvas").GetComponent<Transform>(), false);
            hud = GameObject.Find("HUD").GetComponent<HUD>();
            hud.transform.SetAsLastSibling();

            Init();
        }
        #endregion Unity Lifecycle Functions
        //
        // End Unity Lifecycle Functions
        //

        //
        // Player Board State
        //
        #region Player Board State
        public PlayerBoardState GetPlayerBoardState()
        {
            CalculatePlayerBoardState();
            return playerBoardState;
        }

        private void UpdateNumItemSlots()
        {
            foreach (CardHolder cardHolder in itemCardHolders)
            {
                cardHolder.cardPlacedEvent.RemoveListener(OnCardHolderPlaced);
                cardHolder.gameObject.SetActive(false);
            }

            CardSO heroData = GetPlayerBoardState().hero;
            if (heroData != null && heroData.itemCount > 0)
            {
                for (int i = 0; i < heroData.itemCount; i++)
                {
                    itemCardHolders[i].gameObject.SetActive(true);
                    itemCardHolders[i].cardPlacedEvent.AddListener(OnCardHolderPlaced);
                }
            }
        }

        public void CalculatePlayerBoardState()
        {
            // Loop thru cardholders
            // get cardData
            // add it all up? Maybe.
            playerBoardState.hero = heroCardHolder.GetCardData();

            // initialize hero stats
            if (playerBoardState.hero == null)
            {
                Debug.Log("Can't calculate board stats without hero.");
                return;
            }

            //if (playerBoardState.calculatedHeroStats.heroHealth < 0)
            //{
            //    // The hero has died.
            //    heroCardHolder.ClearCardData();
            //    return;
            //}

            playerBoardState.calculatedHeroStats.heroPower = playerBoardState.hero.power;

            playerBoardState.calculatedHeroStats.heroHealth = playerBoardState.hero.health;
            playerBoardState.calculatedHeroStats.heroToughness = playerBoardState.hero.toughness;

            playerBoardState.armor = armorCardHolder.GetCardData();
            if (playerBoardState.armor != null)
            {
                playerBoardState.calculatedHeroStats.heroToughness += playerBoardState.armor.toughness;
            }

            playerBoardState.weapon = weaponCardHolder.GetCardData();
            if (playerBoardState.weapon != null)
            {
                playerBoardState.calculatedHeroStats.heroPower += playerBoardState.weapon.power;
            }

            foreach (CardHolder cardHolder in itemCardHolders)
            {
                CardSO itemCardData = cardHolder.GetCardData();
                if (itemCardData != null)
                {
                    playerBoardState.items.Add(itemCardData);
                    if (itemCardData.power > 0)
                        playerBoardState.calculatedHeroStats.heroPower += itemCardData.power;
                    if (itemCardData.toughness > 0)
                        playerBoardState.calculatedHeroStats.heroToughness += itemCardData.toughness;
                    if (itemCardData.health > 0)
                        playerBoardState.calculatedHeroStats.heroHealth += itemCardData.health;
                }
            }

            // subtract the damage taken so far.
            // note; damage amount should be reset for each new hero
            Debug.Log("Hero Health is " + playerBoardState.calculatedHeroStats.heroHealth + ". We are going to subtract " + playerBoardState.calculatedHeroStats.heroDamage);
            playerBoardState.calculatedHeroStats.heroHealth -= playerBoardState.calculatedHeroStats.heroDamage;

            heroBonusOverlay.SetHeroStats(playerBoardState.calculatedHeroStats);
        }

        private void ClearPlayerBoardState()
        {
            Debug.Log("!!_!_!!_! Clearing Player board state.");
            if (PhotonNetwork.IsConnected)
            {
                playerBoardState.player = this.photonView.Owner;
            }

            playerBoardState.items = new List<CardSO>();
            playerBoardState.deadHeroes = new List<CardSO>();
            playerBoardState.hero = null;
            playerBoardState.armor = null;
            playerBoardState.weapon = null;
            playerBoardState.calculatedHeroStats = new HeroStats();
        }

        public void LogPlayerBoardState()
        {
            Debug.LogFormat("Player {0} ({1}) board state:", this.photonView.Owner.NickName, this.photonView.Owner.ActorNumber);
            CalculatePlayerBoardState();
            if (playerBoardState.hero != null)
                Debug.LogFormat("Hero Name: {0}", playerBoardState.hero.cardName);

            if (playerBoardState.armor != null)
                Debug.LogFormat("Armor Name: {0}", playerBoardState.armor.cardName);

            if (playerBoardState.weapon != null)
                Debug.LogFormat("Weapon Name: {0}", playerBoardState.weapon.cardName);

            if (playerBoardState.items.Count > 0)
            {
                foreach (CardSO item in playerBoardState.items)
                {
                    Debug.LogFormat("Item Name: {0}", item.cardName);
                }
            }

        }
        #endregion Player Board State
        //
        // End Player Board State
        //


        //
        // Game State
        //
        #region Game State
        public GameState GetGameState()
        {
            // PlayerBoard masterClientPlayerBoard = GetMasterClientPlayerBoard();
            // return masterClientPlayerBoard.gameState;
            return gameState;
        }

        public void SetGameState(GameState next)
        {
            if (PhotonNetwork.IsConnected)
            {
                Debug.LogFormat("Setting game state {0} - dispatching instruction to all Boards.", next);
                this.photonView.RPC("RPC_SetGameState", RpcTarget.All, next);
            }
            else
            {
                // gameState = next;
                Debug.Log("Attempting to set game state but connection missing.");
            }
        }

        public void LogGameState()
        {
            Player currentPlayer = this.photonView.Owner;
            Debug.Log("Logging game state...");
            switch (gameState)
            {
                case GameState.PLAYER_TURN:
                    Debug.Log("PLAYER_TURN");
                    break;
                case GameState.OPPONENT_TURN:
                    Debug.Log("OPPONENT_TURN");
                    break;
                case GameState.WAITING_FOR_OPPONENT:
                    Debug.Log("WAITING_FOR_OPPONENT");
                    break;
                case GameState.COMBAT:
                    Debug.Log("COMBAT");
                    break;
                default: break;
            }
            Debug.LogFormat("Current Player: {0} {1}", currentPlayer.NickName, currentPlayer.ActorNumber);
            //foreach (CardDisplay card in cardsInHand)
            //{
            //    Debug.LogFormat("Card in Hand: {0}", card.cardData.cardName);
            //}

            //Debug.Log("Logging player board states...");
            //PlayerBoard[] playerBoards = GetComponentsInChildren<PlayerBoard>();
            //foreach (PlayerBoard playerBoard in playerBoards)
            //{
            //    playerBoard.LogPlayerBoardState();
            //}
        }

        public void BeginTurn()
        {
            Debug.Log("%%% -- BEGIN TURN ---%%%: For player " + this.photonView.Owner.NickName);
            Debug.Log("%%% -- BEGIN TURN ---%%%: First combat occurred? " + bFirstCombatOccurred);
            Debug.Log("%%% -- BEGIN TURN ---%%%: Num cards in hand? " + cardsInHand.Count);

            if (!this.photonView.IsMine)
            {
                Debug.Log("%%% --- This is not my view. I will not draw cards.");
                return;
            }
            //if (bFirstCombatOccurred)
            //{
            int numCardsInHand = cardsInHand.Count;
            if (numCardsInHand >= MAX_CARDS_IN_HAND)
            {
                Debug.Log("%%% -- Already drew the max.");
                return;
            }

            int remainder = MAX_CARDS_IN_HAND - numCardsInHand;
            Debug.Log("%%% -- BEGIN TURN ---%%%: Remainder: " + remainder);
            if (remainder > 0)
            {
                DrawNCards((uint)remainder);
                ResizeCardsInHand();
            }
            //}
        }

        public void ClearItems()
        {
            // Debug.Log("!!! Clearing player items: "  + this.photonView.Owner.NickName);


            PlayerBoard[] playerBoards = GameObject.FindObjectsOfType<PlayerBoard>();
            //Debug.Log("Found " + playerBoards.Length + " player boards.");

            foreach (PlayerBoard pb in playerBoards)
            {
                foreach (CardHolder cardHolder in pb.itemCardHolders)
                {
                    if (cardHolder.GetCardData())
                    {
                        cardHolder.ClearCardData();
                    }
                }
                pb.CalculatePlayerBoardState();
            }
        }

        // TODO: does opponent mean "my opponent" or does it mean player who isn't master client? This is confusing.
        public PlayerBoard GetOpponentsPlayerBoard()
        {

            PlayerBoard[] playerBoards = GameObject.FindObjectsOfType<PlayerBoard>();
            //Debug.Log("Found " + playerBoards.Length + " player boards.");

            foreach (PlayerBoard pb in playerBoards)
            {
                if (pb.photonView.Owner != this.photonView.Owner)
                {
                    // Debug.Log("Found opponents player board.");
                    return pb;
                }
            }

            return null;
        }

        public PlayerBoard GetMasterClientPlayerBoard()
        {
            PlayerBoard[] playerBoards = GameObject.FindObjectsOfType<PlayerBoard>();
            //Debug.Log("Found " + playerBoards.Length + " player boards.");

            foreach (PlayerBoard pb in playerBoards)
            {
                if (pb.photonView.Owner == PhotonNetwork.MasterClient)
                {
                    // Debug.Log("Found Master Client player board.");
                    return pb;
                }
            }

            Debug.Log("GetMasterClientPlayerBoard: no master client found.");
            return null;
        }

        public void CalculateCombatRound()
        {
            Debug.Log("------------- CALCULATE COMBAT ROUND");
            if (!PhotonNetwork.IsMasterClient)
            {
                Debug.Log("Only master client caclulates combat damage, then applies round.");
                return;
            }

            // Get each PlayerBoard
            PlayerBoard myPlayerBoard = this;
            PlayerBoard opponentsPlayerBoard = GetOpponentsPlayerBoard();
            if (opponentsPlayerBoard == null)
            {
                Debug.Log("Unable to find opponent player's board.");
                return;
            }

            // Run combat functions from hero to hero
            CardSO myHero = myPlayerBoard.GetPlayerBoardState().hero;
            CardSO opponentsHero = opponentsPlayerBoard.GetPlayerBoardState().hero;

            if (myHero == null)
            {
                Debug.Log("My Hero is null");
                return;
            }
            if (opponentsHero == null)
            {
                Debug.Log("Opponents hero is null");
                return;
            }

            int myAttackRoll = RollNSidedDie(20) + myHero.power;
            int opponentAttackRoll = RollNSidedDie(20) + opponentsHero.power;


            // calculate results
            // modify hero hit points
            // if hero dies, remove from board
            CombatRound combatRound = new CombatRound();

            // Me
            if (myAttackRoll > 10 + opponentsHero.toughness)
            {
                int damage = (int)System.Math.Round((double)myHero.power / (double)opponentsHero.toughness) + 10; // minimum 10 damage on a hit
                Debug.Log("I hit the opponent for " + damage + " damage!");
                // I hit!
                combatRound.damageToOpponent = damage;
            }
            else
            {
                Debug.Log("I missed the opponent's hero.");
            }


            // Opponent
            if (opponentAttackRoll > 10 + myHero.toughness)
            {
                // They hit!
                Debug.Log("The opponent hit me :(");
                int damage = (int)System.Math.Round((double)opponentsHero.power / (double)myHero.toughness) + 10;
                Debug.Log("the opponent hit me for " + damage + " damage!");
                combatRound.damageToPlayer = damage;
            }
            else
            {
                Debug.Log("The opponent's hero missed me!");
            }

            //// TODO: only for testing
            //combatRound.damageToPlayer = 20;
            //combatRound.damageToOpponent = 20;
            if (PhotonNetwork.IsConnected)
                this.photonView.RPC("RPC_ApplyCombatRound", RpcTarget.All, combatRound.damageToPlayer, combatRound.damageToOpponent);

            Debug.Log("------------- END CALCULATE COMBAT ROUND");
        }

        public bool IsMyTurn()
        {
            if (PhotonNetwork.IsMasterClient && GetGameState() == GameState.PLAYER_TURN)
            {
                return true;
            }
            else if (!PhotonNetwork.IsMasterClient && GetGameState() == GameState.OPPONENT_TURN)
            {
                return true;
            }
            return false;
        }

        #endregion Game State
        //
        // End Game State
        //

        //
        // Player Board Methods
        //
        #region Player Board Methods
        public void Init()
        {
            Debug.Log("%%% --- INIT. Player: " + this.photonView.Owner.NickName);

            ClearPlayerBoardState();

            gameState = GameState.WAITING_FOR_OPPONENT;

            LoadResources();

            DrawHeroes(3);

            DrawNCards(5);

            ResizeCardsInHand();

            // Should show no item slots initially.
            UpdateNumItemSlots();

            UpdateTurnUI();

            AddListeners();

            // Arrange oppoonent on the other side of the screen
            if (PhotonNetwork.IsConnected)
            {
                if (!this.photonView.IsMine)
                {
                    gameObject.transform.Rotate(-180.0f, -180.0f, 0.0f);
                    Vector3 nextPosition = gameObject.transform.localPosition;
                    nextPosition.x += 75.0f;
                    gameObject.transform.localPosition = nextPosition;

                    ShowCardBackfaces();
                }
                else
                {
                    // For mine, set my player name in the HUD
                    string name = this.photonView.Owner.NickName;
                    if (PhotonNetwork.IsMasterClient)
                    {
                        name += " (Host)";
                    }
                    hud.SetPlayerName(name);
                }
            }
        }

        public void StartGame()
        {
            hud.ShowTurnIndicator(this.photonView.Owner.NickName + "'s turn!");
            hud.HideBackToLobbyBtn();
            SetGameState(GameState.PLAYER_TURN);
            UpdateTurnUI();
        }

        void LoadResources()
        {
            CardSO[] heroCardSOs = Resources.LoadAll<CardSO>("Cards/Heroes");
            heroes = new List<CardSO>(heroCardSOs);
            weapons = Resources.LoadAll<CardSO>("Cards/Weapons");
            armor = Resources.LoadAll<CardSO>("Cards/Armor");
            items = Resources.LoadAll<CardSO>("Cards/Items");
        }

        private void UpdateTurnUI()
        {
            if (!PhotonNetwork.IsConnected)
            {
                Debug.Log("!!! Photon is not connected.");
                return;
            }


            // HUD should not be updated from other users view
            //if (!this.photonView.IsMine)
            //{
            //    Debug.Log("!!! Attempting to update Turn UI but it isn't my own view that is requesting it.");
            //    return;
            //}


            // Debug.LogFormat("Update Turn UI. Game State: {0} - Is Master Client? {1}", gameState, PhotonNetwork.IsMasterClient); ;
            if (!IsMyTurn())
            {
                // Debug.Log("!!! It's not my turn. Disabling end turn button.");
                hud.DisableEndTurnBtn();
            }
            else
            {
                // Debug.Log("!!! It's my turn. If hero is placed then we will enable the button.");
                // You must have a hero in place in order to be able to press done.
                if (heroCardHolder.GetCardData() != null)
                {
                    // Debug.Log("!!! Enabling end turn button.");
                    hud.EnableEndTurnBtn();
                }
            }
        }

        private void AddCardToCardHolder(CardSO cardData, CardHolder cardHolder)
        {
            cardHolder.SetCardData(cardData);
        }

        private void AddCardToCardHolder(CardSO cardData, PlayerBoard board)
        {
            switch (cardData.cardType)
            {
                case CardType.HERO:
                    board.heroCardHolder.SetCardData(cardData);
                    break;
                case CardType.WEAPON:
                    board.weaponCardHolder.SetCardData(cardData);
                    break;
                case CardType.ARMOR:
                    board.armorCardHolder.SetCardData(cardData);
                    break;
                case CardType.ITEM:
                    for (int i = 0; i < itemCardHolders.Length; i++)
                    {
                        bool cardHolderEmpty = itemCardHolders[i].IsEmpty();

                        if (cardHolderEmpty)
                        {
                            Debug.Log("Placing item " + cardData.cardName);
                            itemCardHolders[i].SetCardData(cardData);
                            return;
                        }
                        else
                        {
                            Debug.Log("No empty location to place item");
                        }
                    }
                    break;
                default:
                    return;
            }
        }

        private void RemoveCardFromHand(CardSO cardData)
        {
            int index = cardsInHand.FindIndex(
                delegate (CardDisplay card) {
                    return card.cardData == cardData;
                }
            );
            if (index >= 0)
            {
                CardDisplay cd = cardsInHand[index];
                cardsInHand.RemoveAt(index);
                Destroy(cd.gameObject);
                ResizeCardsInHand();
            }
        }


        private void ResizeCardsInHand()
        {
            if (cardsInHand.Count < 1)
            {
                // Cannot resize with less than one card in hand
                return;
            }

            Canvas canvas = GetComponentInParent<Canvas>();
            RectTransform cardTransform = cardsInHand[0].GetComponent<RectTransform>();
            float furthestLeft = -((cardTransform.rect.width * cardsInHand.Count) / 2);

            for (int i = 0; i < cardsInHand.Count; i++)
            {
                CardDisplay cardDisplay = cardsInHand[i];
                cardDisplay.SetScale(new Vector2(2.5f, 2.5f));

                float y = -((canvas.pixelRect.yMax / 4) - cardTransform.rect.height) - 125;
                float x = furthestLeft + (i * cardTransform.rect.width);
                cardDisplay.SetPosition(new Vector2(x, y));
            }
        }

        private void ShowCardBackfaces()
        {
            for (int i = 0; i < cardsInHand.Count; i++)
            {
                CardDisplay cardDisplay = cardsInHand[i];
                cardDisplay.ShowBack();
            }
        }
        #endregion Player Board Methods
        //
        // End Player Board Methods
        //

        //
        // Card Draw Functions
        //
        #region Card Draw Functions
        public CardSO DrawHero()
        {
            System.Random rand = new System.Random();
            int index = rand.Next(0, heroes.Count);

            CardSO cardData = heroes[index];
            //Debug.Log("Drawing Hero: " + cardData.cardName);
            heroes.RemoveAt(index);
            drawnHeroes.Add(cardData);

            return cardData;
        }

        //public void RemoveHeroFromPool(CardSO heroCard)
        //{
        //    int index = heroes.FindIndex(delegate (CardSO card)
        //    {
        //        return card.cardName == heroCard.cardName;
        //    });
        //}

        public void DrawNCards(uint n)
        {
            var deck = CreateDeck();

            System.Random rand = new System.Random();

            while (n-- > 0)
            {
                CardSO cardData = deck[rand.Next(0, deck.Length)];
                GameObject go = Instantiate(cardPrefab, playerHand.transform);
                CardDisplay cardDisplay = go.GetComponent<CardDisplay>();
                cardDisplay.SetCardData(cardData);
                cardsInHand.Add(cardDisplay);
            }
        }

        public void DrawHeroes(int numHeroes)
        {
            while (numHeroes-- > 0)
            {
                CardSO cardData = DrawHero();
                GameObject go = Instantiate(cardPrefab, playerHand.transform);
                CardDisplay cardDisplay = go.GetComponent<CardDisplay>();
                cardDisplay.SetCardData(cardData);
                cardsInHand.Add(cardDisplay);
            }
        }
        #endregion Card Draw Functions
        //
        // End Card Draw Functions
        //

        //
        // Utilities
        //
        #region Utilities
        // FWAG - Functions We All Get
        public int RollNSidedDie(int n)
        {
            System.Random rand = new System.Random();
            int roll = rand.Next(1, n);
            return roll;
        }

        public CardSO[] CreateDeck()
        {
            CardSO[] deck = new CardSO[weapons.Length + armor.Length + (items.Length * NUM_ITEM_CARDS)];
            weapons.CopyTo(deck, 0);
            armor.CopyTo(deck, weapons.Length);
            for (uint i = 0; i < NUM_ITEM_CARDS; i++)
            {
                items.CopyTo(deck, weapons.Length + armor.Length + (items.Length * i));
            }
            return deck;
        }

        public CardSO GetCardByNameAndType(string cardName, CardType cardType)
        {
            switch (cardType)
            {
                case CardType.HERO:
                    foreach (CardSO hero in heroes)
                    {
                        if (hero.cardName == cardName)
                            return hero;
                    }
                    foreach (CardSO hero in drawnHeroes)
                    {
                        if (hero.cardName == cardName)
                            return hero;
                    }
                    break;
                case CardType.ARMOR:
                    foreach (CardSO card in armor)
                    {
                        if (card.cardName == cardName)
                            return card;
                    }
                    break;
                case CardType.WEAPON:
                    foreach (CardSO card in weapons)
                    {
                        if (card.cardName == cardName)
                            return card;
                    }
                    break;
                case CardType.ITEM:
                    foreach (CardSO card in items)
                    {
                        if (card.cardName == cardName)
                            return card;
                    }
                    break;
                default: break;
            }

            return null;
        }
        #endregion Utilities
        //
        // End Utilities
        //

        //
        // Remote Procedure Calls
        //
        #region Remote Procedure Calls
        [PunRPC]
        public void RPC_PlaceCard(string cardName, CardType cardType)
        {
            Debug.Log("RPC_PlaceCard called.");
            // This means an opponent placed a card.
            PlayerBoard[] playerBoards = GameObject.FindObjectsOfType<PlayerBoard>();
            //Debug.Log("Found " + playerBoards.Length + " player boards");
            foreach (PlayerBoard playerBoard in playerBoards)
            {
                if (!playerBoard.photonView.IsMine)
                {
                    Debug.Log("Found player board that isn't yours.");
                    CardSO cardData = GetCardByNameAndType(cardName, cardType);
                    if (cardData != null)
                    {
                        playerBoard.AddCardToCardHolder(cardData, playerBoard);
                        playerBoard.CalculatePlayerBoardState();
                        // Set hero card data
                        playerBoard.heroCardHolder.SetCardData(playerBoard.GetPlayerBoardState().hero);
                        switch (cardData.cardType)
                        {
                            case CardType.HERO:
                                playerBoard.UpdateNumItemSlots();
                                UpdateTurnUI();
                                break;
                            case CardType.WEAPON:
                                break;
                            case CardType.ARMOR:
                                break;
                            case CardType.ITEM:
                                // It's possible items give you more item slots.
                                playerBoard.UpdateNumItemSlots();
                                break;
                            default: break;
                        }
                    }
                    else
                    {
                        Debug.LogFormat("Card with name {0} not found.");
                    }
                }

            }
        }


        // Player = Master Client
        [PunRPC]
        public void RPC_BeginPlayerTurn()
        {
            Debug.Log("%%% --- RPC_BeginPlayerTurn executing for " + this.photonView.Owner.NickName);
            BeginTurn();
        }

        [PunRPC]
        // Opponent = !Master Client
        public void RPC_BeginOpponentTurn()
        {
            Debug.Log("%%% --- RPC_BeginOpponentTurn executing for " + this.photonView.Owner.NickName);
            BeginTurn();
        }

        [PunRPC]
        public void RPC_SetGameState(GameState next)
        {
            Debug.LogFormat("!!! RPC_SetGameState::Setting game state from {0} to {1} via RPC call for player {2}.", gameState, next, this.photonView.Owner.NickName);
            gameState = next;

            PlayerBoard opponentsBoard = GetOpponentsPlayerBoard();
            if (opponentsBoard != null)
            {
                opponentsBoard.gameState = next;
            }
            else
            {
                Debug.Log("!!! Unable to get opponent players board to sync state.");
            }

            PlayerBoard playersBoard = GetMasterClientPlayerBoard();

            //bool isMyTurn = IsMyTurn();
            //bool isPlayer = PhotonNetwork.IsMasterClient;
            //bool isOpponent = !PhotonNetwork.IsMasterClient;

            string turnIndicatorText = ""; // isMyTurn ? "Your turn, " + this.photonView.Owner.NickName + "!" : opponentsBoard.photonView.Owner.NickName + "'s turn!"; ;

            // if we want to show a message without timer, set to false.
            bool useTimer = true;
            switch (gameState)
            {
                case GameState.PLAYER_TURN:
                    //playersBoard.BeginTurn();
                    //opponentsBoard.BeginTurn();
                    Debug.Log("%%% --- Emitting RPC_BeginPlayerTurn from player " + this.photonView.Owner.NickName);
                    this.photonView.RPC("RPC_BeginPlayerTurn", RpcTarget.All);
                    turnIndicatorText = playersBoard.photonView.Owner.NickName + "'s turn!"; // isPlayer && isMyTurn ? "Your turn, " + this.photonView.Owner.NickName + "!" : opponentsBoard.photonView.Owner.NickName + "'s turn!";
                    break;
                case GameState.OPPONENT_TURN:
                    //playersBoard.BeginTurn();
                    //opponentsBoard.BeginTurn();
                    Debug.Log("%%% --- Emitting RPC_BeginOpponentTurn from player " + this.photonView.Owner.NickName);
                    opponentsBoard.photonView.RPC("RPC_BeginOpponentTurn", RpcTarget.All);
                    turnIndicatorText = opponentsBoard.photonView.Owner.NickName + "'s turn!"; // isOpponent && isMyTurn ? "Your turn, " + this.photonView.Owner.NickName + "!" : opponentsBoard.photonView.Owner.NickName + "'s turn!";
                    break;
                case GameState.COMBAT:
                    turnIndicatorText = "COMBAT!";
                    CalculateCombatRound();
                    ClearItems();
                    break;
                case GameState.WAITING_FOR_OPPONENT:
                    turnIndicatorText = "Waiting for opponent...";
                    useTimer = false;
                    break;
                case GameState.PLAYER_WIN:
                    turnIndicatorText = playersBoard.photonView.Owner.NickName + " has the Last hero Standing!";
                    useTimer = false;
                    hud.ShowBackToLobbyBtn();
                    break;
                case GameState.OPPONENT_WIN:
                    turnIndicatorText = opponentsBoard.photonView.Owner.NickName + " has the Last hero Standing!";
                    useTimer = false;
                    hud.ShowBackToLobbyBtn();
                    break;
                case GameState.DRAW:
                    turnIndicatorText = "No hero is the Last hero Standing... it's a draw!";
                    useTimer = false;
                    hud.ShowBackToLobbyBtn();
                    break;
                default: break;
            }

            hud.ShowTurnIndicator(turnIndicatorText, useTimer);

            UpdateTurnUI();

            // LogGameState();
        }


        [PunRPC]
        public void RPC_ApplyCombatRound(int damageToPlayer, int damageToOpponent)
        {
            Debug.LogFormat("------------- RPC_ApplyCombatRound. Damage to player is {0}, to opponent is {1}", damageToPlayer, damageToOpponent);
            // Flag that we're not on the first round anymore
            if (!bFirstCombatOccurred)
            {
                bFirstCombatOccurred = true;
            }

            CombatRound combatRound = new CombatRound();
            combatRound.damageToPlayer = damageToPlayer;
            combatRound.damageToOpponent = damageToOpponent;

            // Get each PlayerBoard
            PlayerBoard myPlayerBoard = this;
            PlayerBoard opponentsPlayerBoard = GetOpponentsPlayerBoard();
            if (opponentsPlayerBoard == null)
            {
                Debug.Log("Unable to find opponent player's board.");
                return;
            }

            // Run combat functions from hero to hero
            //PlayerBoardState myState = myPlayerBoard.GetPlayerBoardState();
            //PlayerBoardState opponentsState = opponentsPlayerBoard.GetPlayerBoardState();
            if (combatRound.damageToPlayer > 0)
            {
                Debug.Log("" + combatRound.damageToPlayer + " damage to player's hero " + playerBoardState.hero.cardName);

                myPlayerBoard.playerBoardState.calculatedHeroStats.heroDamage += combatRound.damageToPlayer;

                //if (myState.calculatedHeroStats.heroHealth <= 0)
                //{
                //    Debug.Log("my hero is dead!");
                //}

            }

            if (combatRound.damageToOpponent > 0)
            {
                Debug.Log("" + combatRound.damageToOpponent + " damage to opponents's hero " + opponentsPlayerBoard.playerBoardState.hero.cardName);
                opponentsPlayerBoard.playerBoardState.calculatedHeroStats.heroDamage += combatRound.damageToOpponent;

                //if (opponentsState.calculatedHeroStats.heroHealth <= 0)
                //{
                //    Debug.Log("opponents hero is dead!");
                //}
            }

            // TODO: add a short wait?
            // We need to do this to apply the damage. Also a hero might die now.
            myPlayerBoard.CalculatePlayerBoardState();
            opponentsPlayerBoard.CalculatePlayerBoardState();
            if (myPlayerBoard.playerBoardState.calculatedHeroStats.heroHealth < 0)
            {
                // The hero has died.
                myPlayerBoard.playerBoardState.calculatedHeroStats.heroDamage = 0;
                myPlayerBoard.playerBoardState.deadHeroes.Add(myPlayerBoard.heroCardHolder.GetCardData());
                myPlayerBoard.heroCardHolder.ClearCardData();
                myPlayerBoard.heroBonusOverlay.Clear();
            }
            if (opponentsPlayerBoard.playerBoardState.calculatedHeroStats.heroHealth < 0)
            {
                // The hero has died.
                opponentsPlayerBoard.playerBoardState.calculatedHeroStats.heroDamage = 0;
                opponentsPlayerBoard.playerBoardState.deadHeroes.Add(opponentsPlayerBoard.heroCardHolder.GetCardData());
                opponentsPlayerBoard.heroCardHolder.ClearCardData();
                opponentsPlayerBoard.heroBonusOverlay.Clear();
            }

            if (opponentsPlayerBoard.playerBoardState.deadHeroes.Count == 3 && myPlayerBoard.playerBoardState.deadHeroes.Count == 3)
            {
                SetGameState(GameState.DRAW);
                return;
            }
            if (opponentsPlayerBoard.playerBoardState.deadHeroes.Count == 3)
            {
                SetGameState(GameState.PLAYER_WIN);
                return;
            }
            else if (myPlayerBoard.playerBoardState.deadHeroes.Count == 3)
            {
                SetGameState(GameState.OPPONENT_WIN);
                return;
            }

            // after combat it always becomes the hosts turn
            SetGameState(GameState.PLAYER_TURN);

            Debug.Log("------------- END RPC_ApplyCombatRound");
        }
        #endregion Remote Procedure Calls
        //
        // End Remote Procedure Calls
        //

        //
        // UnityEvent Handlers
        //
        #region UnityEvent Handlers
        private void AddListeners()
        {
            CardHolder[] cardHolders = GetComponentsInChildren<CardHolder>();
            foreach (CardHolder cardHolder in cardHolders)
            {
                cardHolder.cardPlacedEvent.AddListener(OnCardHolderPlaced);
            }
        }

        private void OnCardHolderPlaced(CardSO cardData, CardHolder cardHolder, Player player)
        {
            AddCardToCardHolder(cardData, cardHolder);
            RemoveCardFromHand(cardData);
            CalculatePlayerBoardState();

            // Set hero card data post calculations
            heroCardHolder.SetCardData(playerBoardState.hero);

            switch (cardData.cardType)
            {
                case CardType.HERO:
                    UpdateNumItemSlots();
                    UpdateTurnUI();
                    break;
                case CardType.WEAPON:
                    break;
                case CardType.ARMOR:
                    break;
                case CardType.ITEM:
                    // It's possible items give you more item slots.
                    UpdateNumItemSlots();
                    break;
                default: break;
            }

            // Notify others
            if (PhotonNetwork.IsConnected)
                this.photonView.RPC("RPC_PlaceCard", RpcTarget.Others, cardData.cardName, cardData.cardType);
        }
        #endregion UnityEvent Handlers
        //
        // End UnityEvent Handlers
        //
    }
}
