﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace com.sporeservant.lastherostanding
{
    public class HUD : MonoBehaviour
    {
        public GameObject gameStateIndicator;
        public Text gameStateIndicatorText;
        public Text playerName;
        public Button endTurnBtn;
        public Button leaveGameBtn;
        public Button backToLobbyBtn;

        private void Awake()
        {
            DisableEndTurnBtn();
            HideBackToLobbyBtn();
        }

        public void SetPlayerName(string name)
        {
            playerName.text = name;
        }

        public void EnableEndTurnBtn()
        {
            endTurnBtn.GetComponent<Button>().interactable = true;
        }

        public void DisableEndTurnBtn()
        {
            endTurnBtn.GetComponent<Button>().interactable = false;
        }

        public void ShowTurnIndicator(string text, bool useTimer = true)
        {
            // Debug.Log("Showing turn indicator text: " + text);
            gameStateIndicatorText.text = text;
            gameStateIndicator.SetActive(true);
            if (useTimer != false)
            {
                StartCoroutine(AsyncHideTurnIndicator());
            }
        }

        public void HideTurnIndicator()
        {
            gameStateIndicatorText.text = "";
            gameStateIndicator.SetActive(false);
        }

        IEnumerator AsyncHideTurnIndicator()
        {
            yield return new WaitForSeconds(5);
            HideTurnIndicator();
        }

        public void ShowBackToLobbyBtn()
        {
            backToLobbyBtn.gameObject.SetActive(true);
        }

        public void HideBackToLobbyBtn()
        {
            backToLobbyBtn.gameObject.SetActive(false);
        }
    }

}
