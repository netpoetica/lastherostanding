﻿
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Photon.Pun;

namespace com.sporeservant.lastherostanding
{
    public class CardHolder : MonoBehaviour, IDropHandler
    {
        [SerializeField]
        public CardType cardHolderType;

        // the image used to display the card holder
        [SerializeField]
        private Sprite image;

        public CardDisplay cardDisplay;

        public CardPlacedEvent cardPlacedEvent = new CardPlacedEvent();

        public void Start()
        {
            Image imgContainer = GetComponentInChildren<Image>();
            imgContainer.sprite = image;
            cardDisplay.gameObject.SetActive(false);
        }

        public void SetCardData(CardSO data)
        {
            cardDisplay.SetCardData(data);
            // Make sure to update details
            cardDisplay.UpdateCardDisplay();
            cardDisplay.gameObject.SetActive(true);
        }

        public void ClearCardData()
        {
            cardDisplay.SetCardData(null);
            cardDisplay.gameObject.SetActive(false);
        }

        public CardSO GetCardData()
        {
            return cardDisplay.cardData;
        }

        public bool IsEmpty()
        {
            return cardDisplay.cardData == null;
        }

        public void OnDrop(PointerEventData eventData)
        {
            // Once a card is in place, it can't be replaced.
            if (!IsEmpty() && cardHolderType == CardType.HERO)
            {
                Debug.Log("You cannot replace your hero until they are dead.");
            }

            if (eventData.pointerDrag != null)
            {
                CardDisplay cardDisplay = eventData.pointerDrag.GetComponent<CardDisplay>();

                if (this.cardHolderType != cardDisplay.cardData.cardType)
                {
                    Debug.Log("Attempting to place a card in the wrong type of holder.");
                    return;
                }

                PhotonView photonView = GetComponentInParent<PhotonView>();

                cardPlacedEvent.Invoke(cardDisplay.cardData, this, photonView.Owner);
            }
        }
    }
}
