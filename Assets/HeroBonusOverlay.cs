﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.sporeservant.lastherostanding
{
    public class HeroBonusOverlay : MonoBehaviour
    {
        public HeroBonus healthBonus;
        public HeroBonus powerBonus;
        public HeroBonus armorBonus;

        private HeroStats heroStats;

        // Start is called before the first frame update
        void Start()
        {
            Clear();
        }

        public void Clear()
        {
            healthBonus.gameObject.SetActive(false);
            powerBonus.gameObject.SetActive(false);
            armorBonus.gameObject.SetActive(false);
        }

        public void SetHeroStats(HeroStats _heroStats)
        {
            heroStats = _heroStats;
            if (heroStats.heroHealth > 0)
            {
                healthBonus.text.text = heroStats.heroHealth.ToString();
                healthBonus.gameObject.SetActive(true);
            }
            else
            {
                healthBonus.gameObject.SetActive(false);
            }

            if (heroStats.heroPower > 0)
            {
                powerBonus.text.text = heroStats.heroPower.ToString();
                powerBonus.gameObject.SetActive(true);
            }
            else
            {
                powerBonus.gameObject.SetActive(false);
            }

            if (heroStats.heroToughness > 0)
            {
                armorBonus.text.text = heroStats.heroToughness.ToString();
                armorBonus.gameObject.SetActive(true);
            }
            else
            {
                armorBonus.gameObject.SetActive(false);
            }
        }
    }
}