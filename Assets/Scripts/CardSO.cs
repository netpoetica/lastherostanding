﻿
using UnityEngine;

namespace com.sporeservant.lastherostanding
{
    public enum CardType
    {
        HERO,
        WEAPON,
        ARMOR,
        ITEM
    };

    public enum WeaponType
    {
        AXE,
        HAMMER,
        TWO_SIDED_AXE,
        SWORD,
        DAGGER,
        BOW_AND_ARROW,
        TOOLS,
        PRACTICE_SWORD,
        WAND
    };

    public enum ArmorType
    {
        SHIELD,
        HELMET,
        ARMOR
    }

    [CreateAssetMenu(fileName = "New Card", menuName = "Card")]
    public class CardSO : ScriptableObject
    {
        public string cardName;
        public string cardDescription;
        public Sprite cardImage;
        public CardType cardType;

        public int health;
        public int power;
        public int toughness;
        public int itemCount;
    }
}
