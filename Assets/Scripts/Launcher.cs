﻿
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

namespace com.sporeservant.lastherostanding
{
    public class Launcher : MonoBehaviourPunCallbacks
    {
        bool isConnecting;
        string gameVersion = "1.0"; // client version

        [SerializeField]
        private byte maxPlayersPerRoom = 2;

        [SerializeField]
        private GameObject controlPanel;

        [SerializeField]
        private GameObject progressLabel;

        private void Awake()
        {
            // enable LoadLevel fn
            PhotonNetwork.AutomaticallySyncScene = true;
        }

        public void Start()
        {
            Debug.Log("Launcher::start");
            progressLabel.SetActive(false);
            controlPanel.SetActive(true);
        }

        public void Connect()
        {
            isConnecting = true;

            progressLabel.SetActive(true);
            controlPanel.SetActive(false);

            if (PhotonNetwork.IsConnected)
            {
                Debug.Log("Joining random room.");
                PhotonNetwork.JoinRandomRoom();
            }
            else
            {
                Debug.Log("Not connected yet - connecting with settings.");
                PhotonNetwork.GameVersion = gameVersion;
                PhotonNetwork.ConnectUsingSettings();
            }
        }

        public override void OnConnectedToMaster()
        {
            base.OnConnectedToMaster();
            Debug.Log("Connected to photon master callback");
            // after connecting, join room.
            if (isConnecting)
            {
                Debug.Log("Connected to master callback, joining random room.");
                PhotonNetwork.JoinRandomRoom();
            }
        }

        public override void OnDisconnected(DisconnectCause cause)
        {
            base.OnDisconnected(cause);
            Debug.LogWarningFormat("Disconnected from photon master with reason {0}", cause);
            progressLabel.SetActive(false);
            controlPanel.SetActive(true);
        }

        public override void OnJoinedRoom()
        {
            base.OnJoinedRoom();
            Debug.Log("Room joined by client.");

            if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
            {
                Debug.Log("Loading Board scene...");
                PhotonNetwork.LoadLevel("Board");
            }
        }

        public override void OnJoinRandomFailed(short returnCode, string message)
        {
            base.OnJoinRandomFailed(returnCode, message);
            Debug.Log("Unable to find a random room to join. We will create one instead.");
            PhotonNetwork.CreateRoom(null, new RoomOptions { MaxPlayers = maxPlayersPerRoom });
        }
    }
}

